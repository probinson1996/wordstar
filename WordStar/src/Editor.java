import java.util.LinkedList;
import java.util.Scanner;

public class Editor 
{
	public static Scanner responder = new Scanner(System.in);
	public static Document document;
	
	public static void main(String[] args)
	{
		document = new Document();
		awaitResponse();
		init();
	}
	public static void init() 
	{
			String command = getResponse();
			if ((command.length() > 9))
			{	
				if (command.substring(0,8).equals("WordStar"))
					{
						document.setName(command.substring(9, command.length()));
						awaitResponse();
						checkResponse(getResponse());
					}
			}	
			else
				init();
		
	}
	public static void awaitResponse()
	{
		System.out.print(">");
	}
	public static String getResponse()
	{
		return responder.nextLine();
	}
	public static void responseFailed() //delete, debugging tool.
	{
		System.out.println("Invalid command.");
	}
	
	public static void checkResponse(String command)
	{
		if (command == null)
		{
			awaitResponse();
			responseFailed();
			checkResponse(getResponse());
			return;
		}
		if(checkI(command))
		{
			awaitResponse();
			checkResponse(getResponse());
			return;
		}
		if(checkD(command))
		{
			awaitResponse();
			checkResponse(getResponse());
			return;
		}
		if(checkL(command))
		{
			awaitResponse();
			checkResponse(getResponse());
			return;
		}
		if(checkX(command))
		{
			return;
		}
		awaitResponse();
		checkResponse(getResponse());
		
				
		}
	public static boolean checkI(String command)
	{
		Boolean check = false;
		if(command.length() != 0 && command.substring(0,1).equals("I"))
		{
			check = true;
			if (command.length() >= 3)
			{
				if (isInteger(command.substring(2,command.length())))
				{	
					System.out.print("Enter a line of text >");
					document.addLine(getResponse());
				}
				else
				{
					responseFailed();
				}
			}
			else
			{
				System.out.print("Enter a line of text >");
				document.addLine(getResponse());
			}	
		}	
		return check;
	}
	
	public static boolean checkD(String command)
	{
		Boolean check = false;
		if(command.length() != 0 && command.substring(0,1).equals("D"))
		{
			check = true;
			if((command.length() >= 3) && (isInteger(command.substring(2,command.length()))))
				document.deleteLine(Integer.parseInt(command.substring(2,command.length())));
			else
				responseFailed();
		}
		return check;
	}
	public static boolean checkL(String command)
	{
		boolean check = false;
		if(command.length() != 0 && command.substring(0,1).equals("L"))
		{
			check = true;
			System.out.println("Current contents of your document:");
			System.out.println(document.getName());
			LinkedList list = document.getList();
			for(int i = 0; i < list.size(); i++)
			{
				System.out.println(i + ": " + list.get(i));
			}
		}
		return check;
	}
	
	public static boolean checkX(String command)
	{
		boolean check = false;
		if(command.length() != 0 && command.substring(0,1).equals("X"))
		{
			if(command.length() >= 2 + document.getName().length())
			{
				if (command.substring(2,2+ document.getName().length()).equals(document.getName()))
				{
					System.out.println("Saving " + document.getName() + " and exiting...");
					check = true;
				}
			}
					
				
		}
		return check;
	}
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}
	
}
