import java.util.Iterator;
import java.util.LinkedList;

@SuppressWarnings("rawtypes")

public class Document implements UnorderedListADT
{
	String documentName;
	LinkedList list;
	
	public Document()
	{
		documentName = "";	
		list = new LinkedList();
	}


	public void addLine(String response) {
		list.add(response);
		
	}

	public void addLine(String response, int a) {
		if (a >= list.size())
		{
			list.add(response);
		}
		else
			list.add(a, response);
		
	}

	public void deleteLine(int a) {
		if (a >= list.size())
			return;
		else
			list.remove(a);
		
	}

	public String getName() {
		return documentName;
	}

	public LinkedList getList() {
		return list;
	}

	public void setName(String a) {
		documentName = a;
		
	}

	@Override
	public void removeFirst() {
		list.remove(0);
	}

	@Override
	public void removeLast() {
		list.remove(list.size());
	}

	@Override
	public void remove(Object element) {
		for (int i = 0; i < list.size(); i++)
		{
			if (list.get(i).equals(element))
			{
				list.remove(i);
			}
			
		}
	}

	public Object first() {
		return list.getFirst();
	}

	public Object last() {
		return list.getLast();
	}

	public boolean contains(Object target) {
		for (int i = 0; i < list.size(); i++)
		{
			if (list.get(i).equals(target))
			{
				return true;
			}
			
		}
		return false;
	}

	@Override
	public boolean isEmpty() 
	{
	if (list == null)
		return true;
	else
		return false;
	}

	@Override
	public int size() {
		return list.size();
	}


	@Override
	public void addToFront(Object element) {
		list.add(0, element);
		
	}

	@Override
	public void addToRear(Object element) {
		list.add(element);
		
	}

	@Override
	public void addAfter(Object element, Object target) {
		for (int i = 0; i < list.size(); i++)
		{
			if (list.get(i).equals(target))
			{
				list.add(i, element);
			}
			
		}
		
		
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}


